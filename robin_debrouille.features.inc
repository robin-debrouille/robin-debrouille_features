<?php
/**
 * @file
 * robin_debrouille.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function robin_debrouille_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels" && $api == "layouts") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function robin_debrouille_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function robin_debrouille_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>articles</em> pour des contenus possédant une temporalité tels que des actualités, des communiqués de presse ou des billets de blog.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'astuce' => array(
      'name' => t('Astuces'),
      'base' => 'node_content',
      'description' => t('tous les astuces cuisine, bricolage, jardinage,informatique...'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'etape' => array(
      'name' => t('Étape'),
      'base' => 'node_content',
      'description' => t('toutes les étapes pour bon tuto'),
      'has_title' => '1',
      'title_label' => t('titre de l\'étape'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page de base'),
      'base' => 'node_content',
      'description' => t('Utilisez les <em>pages de base</em> pour votre contenu statique, tel que la page \'Qui sommes-nous\'.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'partenaire' => array(
      'name' => t('Partenaire'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
