<?php
/**
 * @file
 * robin_debrouille.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function robin_debrouille_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 4,
  );

  // Exported role: moderateur.
  $roles['moderateur'] = array(
    'name' => 'moderateur',
    'weight' => 3,
  );

  // Exported role: partenaires.
  $roles['partenaires'] = array(
    'name' => 'partenaires',
    'weight' => 2,
  );

  return $roles;
}
