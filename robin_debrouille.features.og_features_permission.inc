<?php
/**
 * @file
 * robin_debrouille.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function robin_debrouille_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:partenaire:add user'
  $permissions['node:partenaire:add user'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:partenaire:administer group'
  $permissions['node:partenaire:administer group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:partenaire:approve and deny subscription'
  $permissions['node:partenaire:approve and deny subscription'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:partenaire:create astuce content'
  $permissions['node:partenaire:create astuce content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:partenaire:delete any astuce content'
  $permissions['node:partenaire:delete any astuce content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:partenaire:delete own astuce content'
  $permissions['node:partenaire:delete own astuce content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:partenaire:manage members'
  $permissions['node:partenaire:manage members'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:partenaire:manage permissions'
  $permissions['node:partenaire:manage permissions'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:partenaire:manage roles'
  $permissions['node:partenaire:manage roles'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:partenaire:subscribe'
  $permissions['node:partenaire:subscribe'] = array(
    'roles' => array(
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:partenaire:subscribe without approval'
  $permissions['node:partenaire:subscribe without approval'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:partenaire:unsubscribe'
  $permissions['node:partenaire:unsubscribe'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:partenaire:update any astuce content'
  $permissions['node:partenaire:update any astuce content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:partenaire:update group'
  $permissions['node:partenaire:update group'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:partenaire:update own astuce content'
  $permissions['node:partenaire:update own astuce content'] = array(
    'roles' => array(),
  );

  return $permissions;
}
