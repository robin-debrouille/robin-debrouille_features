<?php
/**
 * @file
 * robin_debrouille.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function robin_debrouille_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['comment-recent'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'recent',
    'module' => 'comment',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -16,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -16,
      ),
      'seven' => array(
        'region' => 'dashboard_inactive',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['devel-execute_php'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'execute_php',
    'module' => 'devel',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -12,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -12,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['devel-switch_user'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'switch_user',
    'module' => 'devel',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -2,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -2,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['devel_node_access-dna_node'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'dna_node',
    'module' => 'devel_node_access',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => 'bottom',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => -20,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['devel_node_access-dna_user'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'dna_user',
    'module' => 'devel_node_access',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -14,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -14,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['forum-active'] = array(
    'cache' => -2,
    'custom' => 0,
    'delta' => 'active',
    'module' => 'forum',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -3,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -3,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 3,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['forum-new'] = array(
    'cache' => -2,
    'custom' => 0,
    'delta' => 'new',
    'module' => 'forum',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -5,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -5,
      ),
      'seven' => array(
        'region' => 'dashboard_inactive',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 1,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['hybridauth-hybridauth'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'hybridauth',
    'module' => 'hybridauth',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['locale-language'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'language',
    'module' => 'locale',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['masquerade-masquerade'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'masquerade',
    'module' => 'masquerade',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => 'bottom',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => -19,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-devel'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'devel',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -13,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -13,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-features'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'features',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -10,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -10,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-footer'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-footer',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => -18,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['menu-menu-menu-segondaire'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-menu-segondaire',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-moderateur'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-moderateur',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'moderateur' => 4,
    ),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => 'bottom',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['node-recent'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'recent',
    'module' => 'node',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -15,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -15,
      ),
      'seven' => array(
        'region' => 'dashboard_main',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['node-syndicate'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'syndicate',
    'module' => 'node',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -1,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -1,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['print-print-links'] = array(
    'cache' => 4,
    'custom' => 0,
    'delta' => 'print-links',
    'module' => 'print',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['print-print-top'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'print-top',
    'module' => 'print',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['print_mail-print_mail-top'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'print_mail-top',
    'module' => 'print_mail',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['print_pdf-print_pdf-top'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'print_pdf-top',
    'module' => 'print_pdf',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['profile-author-information'] = array(
    'cache' => 5,
    'custom' => 0,
    'delta' => 'author-information',
    'module' => 'profile',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'administrator' => 3,
      'moderateur' => 4,
      'partenaires' => 5,
      'utilisateur anonyme' => 1,
      'utilisateur authentifié' => 2,
    ),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'navigation',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => -1,
      ),
      'robinDebrouille' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => -1,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Rechercher',
    'visibility' => 0,
  );

  $export['shortcut-shortcuts'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'shortcuts',
    'module' => 'shortcut',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -4,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -4,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['statistics-popular'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'popular',
    'module' => 'statistics',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-help'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'help',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => -21,
      ),
      'seven' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => -20,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -8,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -8,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-management'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'management',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -9,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -9,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-navigation'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'navigation',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -7,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -7,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-powered-by'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'powered-by',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 10,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 10,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 10,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 1,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 1,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => 'navigation',
        'status' => 1,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 10,
      ),
    ),
    'title' => 'Se connecter',
    'visibility' => 0,
  );

  $export['user-new'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'new',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => -6,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -6,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 1,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-online'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'online',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 2,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 2,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 2,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-astuces-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'astuces-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-astuces-block_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'astuces-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-astuces-block_3'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'astuces-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-categorie_menu-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'categorie_menu-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'astuces
categories_astuces/*',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => 'highlighted',
        'status' => 1,
        'theme' => 'robinDebrouille',
        'weight' => -22,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-menu_categorie-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu_categorie-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => -22,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-og_members-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'og_members-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'corporateclean' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'corporateclean',
        'weight' => 0,
      ),
      'robinDebrouille' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'robinDebrouille',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
